package shop.velox.commons.security;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;

import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

@ExtendWith(MockitoExtension.class)
class VeloxNimbusOpaqueTokenIntrospectorTest {

  private static final String SCOPE_AUTHORITY_PREFIX = "SCOPE_the:urn:";

  private VeloxNimbusOpaqueTokenIntrospector veloxNimbusOpaqueTokenIntrospector;

  @BeforeEach
  void setUp() {
    veloxNimbusOpaqueTokenIntrospector = new VeloxNimbusOpaqueTokenIntrospector("uri", "clientId",
        "clientSecret", SCOPE_AUTHORITY_PREFIX);
  }

  @Test
  void convert() {
    // Given
    var adminRole = "Admin";
    var authorities = Stream
        .of("some_other_authority", SCOPE_AUTHORITY_PREFIX + adminRole)
        .map(SimpleGrantedAuthority::new)
        .collect(Collectors.toSet());

    // When
    var result = veloxNimbusOpaqueTokenIntrospector.convert(authorities);

    // Then
    assertThat(result, contains(new SimpleGrantedAuthority(adminRole)));
  }
}