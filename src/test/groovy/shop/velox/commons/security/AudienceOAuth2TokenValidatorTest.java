package shop.velox.commons.security;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Map;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.oauth2.core.OAuth2TokenValidatorResult;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtClaimNames;

@ExtendWith(MockitoExtension.class)
class AudienceOAuth2TokenValidatorTest {

  private static final String AUDIENCE = "69507892323147790@velox_local";

  private AudienceOAuth2TokenValidator audienceOAuth2TokenValidator;

  @BeforeEach
  void setUp() {
    audienceOAuth2TokenValidator = new AudienceOAuth2TokenValidator(AUDIENCE);
  }

  @Test
  void validate() {
    // Given
    Jwt jwt = new Jwt("the token value2", Instant.now().minus(1, ChronoUnit.HOURS),
        Instant.now().plus(1, ChronoUnit.HOURS), Map.of("typ", "JWT"),
        Map.of(JwtClaimNames.AUD, AUDIENCE));

    // When
    OAuth2TokenValidatorResult result = audienceOAuth2TokenValidator.validate(jwt);

    // Then
    assertFalse(result.hasErrors());
  }

  @Test
  void validateError() {
    // Given
    Jwt jwt = new Jwt("the token value2", Instant.now().minus(1, ChronoUnit.HOURS),
        Instant.now().plus(1, ChronoUnit.HOURS), Map.of("typ", "JWT"),
        Map.of(JwtClaimNames.AUD, "other_audience"));

    // When
    OAuth2TokenValidatorResult result = audienceOAuth2TokenValidator.validate(jwt);

    // Then
    assertTrue(result.hasErrors());
    assertThat(result, hasProperty("errors", Matchers.contains(
        allOf(
            hasProperty("errorCode", is("invalid_token")),
            hasProperty("description", is("The required audience is missing")),
            hasProperty("uri", nullValue())
        )
    )));
  }

}
