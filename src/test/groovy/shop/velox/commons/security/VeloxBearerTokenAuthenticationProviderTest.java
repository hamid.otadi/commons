package shop.velox.commons.security;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.jwt.BadJwtException;
import org.springframework.security.oauth2.server.resource.BearerTokenAuthenticationToken;
import org.springframework.security.oauth2.server.resource.InvalidBearerTokenException;
import org.springframework.security.oauth2.server.resource.authentication.BearerTokenAuthentication;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;

@ExtendWith(MockitoExtension.class)
class VeloxBearerTokenAuthenticationProviderTest {

  private VeloxBearerTokenAuthenticationProvider veloxBearerTokenAuthenticationProvider;

  @Mock
  private AuthenticationProvider jwtAuthenticationProvider;

  @Mock
  private AuthenticationProvider opaqueTokenAuthenticationProvider;

  @BeforeEach
  void setUp() {
    veloxBearerTokenAuthenticationProvider = new VeloxBearerTokenAuthenticationProvider(
        jwtAuthenticationProvider, opaqueTokenAuthenticationProvider);
  }

  @Test
  void authenticate() {
    // Given
    var bearerTokenAuthenticationToken = new BearerTokenAuthenticationToken("the-token-value");

    var jwtAuthentication = mock(JwtAuthenticationToken.class);
    when(jwtAuthenticationProvider.authenticate(bearerTokenAuthenticationToken))
        .thenReturn(jwtAuthentication);

    // When
    var result = veloxBearerTokenAuthenticationProvider
        .authenticate(bearerTokenAuthenticationToken);

    // Then
    assertEquals(jwtAuthentication, result);
  }

  @Test
  void authenticateOpaqueToken() {
    // Given
    var bearerTokenAuthenticationToken = new BearerTokenAuthenticationToken("the-token-value");

    when(jwtAuthenticationProvider.authenticate(bearerTokenAuthenticationToken))
        .thenThrow(new InvalidBearerTokenException("Token invalid",
            new BadJwtException("Invalid format")));

    var bearerTokenAuthentication = mock(BearerTokenAuthentication.class);
    when(opaqueTokenAuthenticationProvider.authenticate(bearerTokenAuthenticationToken))
        .thenReturn(bearerTokenAuthentication);

    // When
    Authentication result = veloxBearerTokenAuthenticationProvider
        .authenticate(bearerTokenAuthenticationToken);

    // Then
    assertEquals(bearerTokenAuthentication, result);
  }

  @Test
  void authenticateException() {
    // Given
    var bearerTokenAuthenticationToken = new BearerTokenAuthenticationToken("the-token-value");

    InvalidBearerTokenException exception = new InvalidBearerTokenException("Token invalid");
    when(jwtAuthenticationProvider.authenticate(bearerTokenAuthenticationToken))
        .thenThrow(exception);

    // When
    Exception result = assertThrows(AuthenticationException.class, () -> {
      veloxBearerTokenAuthenticationProvider.authenticate(bearerTokenAuthenticationToken);
    });

    // Then
    assertEquals(exception, result);
  }

  @Test
  void supports() {
    // Given
    Class<BearerTokenAuthenticationToken> clazz = BearerTokenAuthenticationToken.class;
    when(jwtAuthenticationProvider.supports(clazz)).thenReturn(true);

    // When
    boolean result = veloxBearerTokenAuthenticationProvider.supports(clazz);

    // Then
    assertTrue(result);
  }

}