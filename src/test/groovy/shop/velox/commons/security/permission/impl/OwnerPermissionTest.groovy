package shop.velox.commons.security.permission.impl

import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken
import org.springframework.security.oauth2.core.user.OAuth2User
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken
import shop.velox.commons.security.annotation.OwnerId
import spock.lang.Specification
import spock.lang.Unroll

class OwnerPermissionTest extends Specification {
    static DEFAULT_OWNER = "A default owner" + new Random().nextLong()
    def permission = new OwnerPermission()
    def domainObject = getMockedDomainObject(DEFAULT_OWNER)

    def "Authentication is accepted as JwtAuthenticationToken class"() {
        given:
        def auth = Mock(JwtAuthenticationToken) {
            getName() >> DEFAULT_OWNER
        }
        expect:
        permission.isAllowed(auth, domainObject)
    }

    def "Authentication is accepted as OAuth2AuthenticationToken class"() {
        given:
        def auth = Mock(OAuth2AuthenticationToken) {
            getPrincipal() >> Mock(OAuth2User) {
                getAttribute(_) >> DEFAULT_OWNER
            }
        }
        expect:
        permission.isAllowed(auth, domainObject)
    }

    @Unroll
    def "Permission verification result for '#principal' to entity owned by '#entityOwner' should be #result"() {
        given:
        def auth = Mock(JwtAuthenticationToken) {
            getName() >> principal
        }
        expect:
        result == permission.isAllowed(auth, new DomainObject(cartOwner))
        where:
        cartOwner         | principal        || result
        "1"               | "1"              || true
        "me@velox.local"  | "me@velox.local" || true
        "me@velox.local"  | "1"              || false
        "me2@velox.local" | "me@velox.local" || false
        "me@velox.local"  | null             || false
        null              | "me@velox.local" || false
    }

    private class DomainObject {
        static OWNER;

        DomainObject(owner) {
            this.OWNER = owner
        }

        @OwnerId
        getOwnerId() {
            return
        }
    }
}
