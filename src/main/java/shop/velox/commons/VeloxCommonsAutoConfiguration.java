package shop.velox.commons;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;


@Configuration
@ComponentScan("shop.velox.commons")
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class VeloxCommonsAutoConfiguration {

}
