package shop.velox.commons.mail.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;
import shop.velox.commons.mail.service.MailService;
import shop.velox.commons.mail.service.MessageBuilder;

@Component("mailService")
@ConfigurationProperties(prefix = "velox.mail")
@ConditionalOnBean(JavaMailSender.class)
public class VeloxMailService implements MailService {

  private JavaMailSender mailSender;
  private String sender;

  public VeloxMailService(@Autowired final JavaMailSender mailSender) {
    this.mailSender = mailSender;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public MessageBuilder getSimpleMessageBuilder() {
    return SimpleMessageBuilder.createSimpleMessageBuilder(mailSender, sender);
  }

  public void setSender(String sender) {
    this.sender = sender;
  }
}
