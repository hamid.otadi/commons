package shop.velox.commons.mail.service;

/**
 * Common Velox mail service
 */
public interface MailService {

  /**
   * Factory method to retrieve plain message builder
   *
   * @return plain message builder
   */
  MessageBuilder getSimpleMessageBuilder();
}
