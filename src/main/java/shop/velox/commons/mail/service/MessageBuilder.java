package shop.velox.commons.mail.service;

import org.springframework.mail.MailSendException;
import shop.velox.commons.mail.service.impl.SimpleMessageBuilder;

/**
 * Chaining message builder
 */
public interface MessageBuilder {

  /**
   * Sets sender address, allowing to owverride default set with 'velox.mail.sender' property.
   *
   * @param mailFrom sender address to set
   * @return this builder for chaining
   */
  SimpleMessageBuilder setMailFrom(String mailFrom);

  /**
   * Adds recipent(s) address(es) for message. Internally stored as set to avoid duplication.
   *
   * @param recipient recipent(s) address(es) to add
   * @return this builder for chaining
   */
  SimpleMessageBuilder addRecipient(String... recipient);

  /**
   * Sets subject for message
   *
   * @param subject subject to set
   * @return this builder for chaining
   */
  SimpleMessageBuilder setSubject(String subject);

  /**
   * Sets message's body
   *
   * @param mailBody body to set
   * @return this builder for chaining
   */
  SimpleMessageBuilder setMailBody(Object mailBody);

  /**
   * Sends message.
   *
   * @throws MailSendException on send errors
   */
  void send();
}
