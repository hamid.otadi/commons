package shop.velox.commons;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

public abstract class AbstractVeloxWebSecurityConfiguration extends WebSecurityConfigurerAdapter {

  protected static final Logger LOG = LoggerFactory
      .getLogger(AbstractVeloxWebSecurityConfiguration.class);

  @Override
  protected void configure(final HttpSecurity http) throws Exception {
    http.sessionManagement(c -> c.sessionCreationPolicy(SessionCreationPolicy.STATELESS));
    http.authorizeRequests()
        .antMatchers("/actuator/health").permitAll()
        .antMatchers("/actuator/**").hasAuthority("Admin");
    http.cors();
    http.csrf().disable();
  }

  @Override
  public void init(WebSecurity web) throws Exception {
    super.init(web);
    logActiveConfiguration(this.getClass());
  }

  protected void logActiveConfiguration(
      final Class<? extends AbstractVeloxWebSecurityConfiguration> clazz) {
    LOG.info("Active Velox web security configuration: {}", clazz.getSimpleName());
  }
}
