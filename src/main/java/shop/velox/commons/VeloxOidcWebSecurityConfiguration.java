package shop.velox.commons;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.security.oauth2.resource.OAuth2ResourceServerProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.core.DelegatingOAuth2TokenValidator;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.OAuth2Error;
import org.springframework.security.oauth2.core.OAuth2TokenValidator;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtDecoders;
import org.springframework.security.oauth2.jwt.JwtValidators;
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder;
import org.springframework.security.oauth2.server.resource.BearerTokenError;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationProvider;
import org.springframework.security.oauth2.server.resource.authentication.OpaqueTokenAuthenticationProvider;
import org.springframework.security.oauth2.server.resource.introspection.OpaqueTokenIntrospector;
import org.springframework.security.oauth2.server.resource.web.BearerTokenAuthenticationEntryPoint;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.util.matcher.MediaTypeRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.web.accept.ContentNegotiationStrategy;
import shop.velox.commons.security.AudienceOAuth2TokenValidator;
import shop.velox.commons.security.VeloxBearerTokenAuthenticationProvider;
import shop.velox.commons.security.VeloxNimbusOpaqueTokenIntrospector;

/**
 * OIDC based oAuth configuration
 */
@EnableWebSecurity
@Order(110)
@ConditionalOnProperty(value = "velox.security.oidc.enabled", havingValue = "true")
public class VeloxOidcWebSecurityConfiguration extends AbstractVeloxWebSecurityConfiguration {

  private static final Logger LOG = LoggerFactory.getLogger(VeloxOidcWebSecurityConfiguration.class);

  @Value("${velox.security.oidc.claim.grantedAuthority}")
  private String authoritiesClaimName;

  @Value("${velox.security.oidc.scopeAuthority.prefix}")
  private String scopeAuthorityPrefix;

  @Autowired
  private OAuth2ResourceServerProperties oAuth2ResourceServerProperties;

  @Override
  protected void configure(final HttpSecurity http) throws Exception {
    super.configure(http);
    http
        .oauth2ResourceServer()
        .authenticationManagerResolver(
            context -> bearerTokenAuthenticationProvider()::authenticate);
    configureResourceServer401ResponseBody(http);
  }

  @Bean
  public AuthenticationProvider bearerTokenAuthenticationProvider() {
    return new VeloxBearerTokenAuthenticationProvider(jwtAuthenticationProvider(),
        opaqueTokenAuthenticationProvider());
  }

  @Bean
  public AuthenticationProvider jwtAuthenticationProvider() {
    NimbusJwtDecoder jwtDecoder = (NimbusJwtDecoder) JwtDecoders
        .fromOidcIssuerLocation(oAuth2ResourceServerProperties.getJwt().getIssuerUri());

    OAuth2TokenValidator<Jwt> defaultWithIssuer = JwtValidators.createDefaultWithIssuer(
        oAuth2ResourceServerProperties.getJwt().getIssuerUri());
    OAuth2TokenValidator<Jwt> defaultWithIssuerAndAudience = new DelegatingOAuth2TokenValidator<>(
        defaultWithIssuer, new AudienceOAuth2TokenValidator(
        oAuth2ResourceServerProperties.getOpaquetoken().getClientId()));
    jwtDecoder.setJwtValidator(defaultWithIssuerAndAudience);

    JwtAuthenticationProvider jwtAuthenticationProvider = new JwtAuthenticationProvider(jwtDecoder);
    jwtAuthenticationProvider.setJwtAuthenticationConverter(jwtAuthenticationConverter());
    return jwtAuthenticationProvider;
  }

  @Bean
  public AuthenticationProvider opaqueTokenAuthenticationProvider() {
    OpaqueTokenIntrospector introspectionClient = new VeloxNimbusOpaqueTokenIntrospector(
        oAuth2ResourceServerProperties.getOpaquetoken().getIntrospectionUri(),
        oAuth2ResourceServerProperties.getOpaquetoken().getClientId(),
        oAuth2ResourceServerProperties.getOpaquetoken().getClientSecret(),
        scopeAuthorityPrefix);
    return new OpaqueTokenAuthenticationProvider(introspectionClient);
  }

  private JwtAuthenticationConverter jwtAuthenticationConverter() {
    // create a custom JWT converter to map the roles from the token as granted authorities
    ZitadelJwtGrantedAuthoritiesConverter jwtGrantedAuthoritiesConverter = new ZitadelJwtGrantedAuthoritiesConverter();
    jwtGrantedAuthoritiesConverter.setAuthoritiesClaimName(authoritiesClaimName);
    jwtGrantedAuthoritiesConverter.setAuthorityPrefix(""); // default is: SCOPE_
    LOG.info("Using JwtAuthenticationConverter: {}", ReflectionToStringBuilder.toString(jwtGrantedAuthoritiesConverter,
        ToStringStyle.JSON_STYLE));

    JwtAuthenticationConverter jwtAuthenticationConverter = new JwtAuthenticationConverter();
    jwtAuthenticationConverter.setJwtGrantedAuthoritiesConverter(jwtGrantedAuthoritiesConverter);
    return jwtAuthenticationConverter;
  }

  public static HttpSecurity configureResourceServer401ResponseBody(HttpSecurity http) throws Exception {
    return http.exceptionHandling()
        .defaultAuthenticationEntryPointFor(authenticationEntryPoint(), textRequestMatcher(http)).and();
  }

  private static AuthenticationEntryPoint authenticationEntryPoint() {
    BearerTokenAuthenticationEntryPoint bearerTokenEntryPoint = new BearerTokenAuthenticationEntryPoint();
    return (request, response, authException) -> {

      LOG.info("{}, {}, {}", request, response, authException);

      response.setContentType(MediaType.TEXT_PLAIN.toString());
      response.getWriter().print(statusAsString(getStatus(authException)));
      bearerTokenEntryPoint.commence(request, response, authException);
    };
  }

  private static RequestMatcher textRequestMatcher(HttpSecurity http) {
    return new MediaTypeRequestMatcher(http.getSharedObject(ContentNegotiationStrategy.class), MediaType.TEXT_PLAIN);
  }

  static String statusAsString(HttpStatus status) {
    return status.value() + " " + status.getReasonPhrase();
  }

  static HttpStatus getStatus(AuthenticationException authException) {
    if (authException instanceof OAuth2AuthenticationException) {
      OAuth2Error error = ((OAuth2AuthenticationException) authException).getError();
      if (error instanceof BearerTokenError) {
        return ((BearerTokenError) error).getHttpStatus();
      }
    }
    return HttpStatus.UNAUTHORIZED;
  }


}
