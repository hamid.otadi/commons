package shop.velox.commons.security;

import static org.springframework.security.oauth2.core.oidc.StandardClaimNames.SUB;

public final class VeloxSecurityConstants {

  public static class Authorities {

    public static final String GLOBAL_ADMIN_AUTHORIZATION = "Admin";
  }

  public static class Oidc {

    public static class ClaimName {

      /**
       *  In Zitadel only sub exists.
       *  In Okta both uid and sub exist. sub is the login name, which can be changed by the user.
       *  // TODO refactor this to be a property. Right now we need this to be static
        */
      public static final String OKTA_CUSTOMER_UID = "uid";
      public static final String CUSTOMER_ID = SUB;
    }
  }

  private VeloxSecurityConstants() {
    // private constructor to prevent instantiation
  }
}
