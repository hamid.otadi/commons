package shop.velox.commons.security.utils;

import static shop.velox.commons.security.VeloxSecurityConstants.Oidc.ClaimName.CUSTOMER_ID;

import java.security.Principal;
import java.util.Optional;
import java.util.function.Function;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;

/**
 * Static class for common authentication and authorization utility methods
 */
public class AuthUtils {

  private static final Function<Jwt, Optional<String>> jwtToCustomerIdMapper = j ->
      Optional.of(j.getClaimAsString(CUSTOMER_ID));

  private static final Function<Object, Optional<String>> userToCustomerIdMapper = u ->
      Optional.of(u)
          .filter(User.class::isInstance)
          .map(User.class::cast)
          .map(User::getUsername);

  private static final Function<Principal, Optional<String>> jwtTokenToCustomerIdMapper = p ->
      Optional.of(p)
          .filter(JwtAuthenticationToken.class::isInstance)
          .map(JwtAuthenticationToken.class::cast)
          .map(JwtAuthenticationToken::getToken)
          .flatMap(jwtToCustomerIdMapper);

  private static final Function<Principal, Optional<String>> oA2TokenToCustomerIdMapper = p ->
      Optional.of(p)
          .filter(OAuth2AuthenticationToken.class::isInstance)
          .map(OAuth2AuthenticationToken.class::cast)
          .map(OAuth2AuthenticationToken::getPrincipal)
          .map(u -> u.getAttribute(CUSTOMER_ID));

  private static final Function<Principal, Optional<String>> usernamePasswordTokenToCustomerIdMapper = p ->
      Optional.of(p)
          .filter(UsernamePasswordAuthenticationToken.class::isInstance)
          .map(UsernamePasswordAuthenticationToken.class::cast)
          .map(UsernamePasswordAuthenticationToken::getPrincipal)
          .flatMap(userToCustomerIdMapper);

  /**
   * User ID extractor from {@link Principal} object, returns null when none can be found
   */
  public static final Function<Object, String> principalToCustomerIdMapper = p -> {
    if (p instanceof Principal) {
      return jwtTokenToCustomerIdMapper.apply((Principal) p)
          .orElseGet(() -> oA2TokenToCustomerIdMapper.apply((Principal) p)
              .orElseGet(() -> usernamePasswordTokenToCustomerIdMapper.apply((Principal) p)
                  .orElse(null)));
    } else if (p instanceof Jwt) {
      return jwtToCustomerIdMapper.apply((Jwt) p).orElse(null);
    } else if (p instanceof User) {
      return userToCustomerIdMapper.apply(p).orElse(null);
    } else {
      return null;
    }
  };

  private AuthUtils() {
    // private constructor to prevent instantiation
  }
}
