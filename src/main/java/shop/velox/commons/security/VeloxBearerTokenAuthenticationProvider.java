package shop.velox.commons.security;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.jwt.BadJwtException;
import org.springframework.security.oauth2.server.resource.InvalidBearerTokenException;

/**
 * This Bearer token authentication provider has a fallback mechanism implemented. It first tries to
 * authenticate the user based on JWT validation. If the provided token is not a JWT then this
 * provider uses an opaque token provider to validate the token with introspection.
 */
public class VeloxBearerTokenAuthenticationProvider implements AuthenticationProvider {

  private final AuthenticationProvider jwtAuthenticationProvider;
  private final AuthenticationProvider opaqueTokenAuthenticationProvider;

  public VeloxBearerTokenAuthenticationProvider(
      AuthenticationProvider jwtAuthenticationProvider,
      AuthenticationProvider opaqueTokenAuthenticationProvider) {
    this.jwtAuthenticationProvider = jwtAuthenticationProvider;
    this.opaqueTokenAuthenticationProvider = opaqueTokenAuthenticationProvider;
  }

  @Override
  public Authentication authenticate(Authentication authentication) throws AuthenticationException {
    try {
      return jwtAuthenticationProvider.authenticate(authentication);
    } catch (InvalidBearerTokenException e) {
      if (e.getCause() instanceof BadJwtException) {
        return opaqueTokenAuthenticationProvider.authenticate(authentication);
      } else {
        throw e;
      }
    }
  }

  @Override
  public boolean supports(Class<?> authentication) {
    return jwtAuthenticationProvider.supports(authentication);
  }
}
