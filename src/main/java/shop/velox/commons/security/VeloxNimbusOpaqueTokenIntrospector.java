package shop.velox.commons.security;

import java.util.Collection;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.core.OAuth2AuthenticatedPrincipal;
import org.springframework.security.oauth2.server.resource.introspection.NimbusOpaqueTokenIntrospector;
import org.springframework.security.oauth2.server.resource.introspection.OAuth2IntrospectionAuthenticatedPrincipal;

/**
 * This extension of the {@link NimbusOpaqueTokenIntrospector} filters and converts authorities
 * based on a prefix.
 */
public class VeloxNimbusOpaqueTokenIntrospector extends NimbusOpaqueTokenIntrospector {

  private final String scopeAuthorityPrefix;

  public VeloxNimbusOpaqueTokenIntrospector(String introspectionUri, String clientId,
      String clientSecret, String scopeAuthorityPrefix) {
    super(introspectionUri, clientId, clientSecret);
    this.scopeAuthorityPrefix = scopeAuthorityPrefix;
  }

  @Override
  public OAuth2AuthenticatedPrincipal introspect(String token) {
    OAuth2AuthenticatedPrincipal oAuth2AuthenticatedPrincipal = super.introspect(token);

    return new OAuth2IntrospectionAuthenticatedPrincipal(
        oAuth2AuthenticatedPrincipal.getAttributes(),
        convert(oAuth2AuthenticatedPrincipal.getAuthorities()));
  }

  Collection<GrantedAuthority> convert(Collection<? extends GrantedAuthority> authorities) {
    return authorities.stream()
        .filter(
            authority -> authority.getAuthority().startsWith(scopeAuthorityPrefix))
        .map(authority -> new SimpleGrantedAuthority(
            StringUtils.removeStart(authority.getAuthority(), scopeAuthorityPrefix)))
        .collect(Collectors.toSet());
  }
}
