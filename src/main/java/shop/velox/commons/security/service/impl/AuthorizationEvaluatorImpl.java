package shop.velox.commons.security.service.impl;

import static shop.velox.commons.security.VeloxSecurityConstants.Authorities.GLOBAL_ADMIN_AUTHORIZATION;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;
import shop.velox.commons.security.service.AuthorizationEvaluator;
import shop.velox.commons.security.utils.AuthUtils;

@Component("veloxAuthorizationEvaluator")
public class AuthorizationEvaluatorImpl implements AuthorizationEvaluator {

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean hasGlobalAdminAuthority(final Authentication authentication) {
    final Predicate<String> isAdminAuthority = a -> Objects.equals(GLOBAL_ADMIN_AUTHORIZATION, a);
    return authentication.getAuthorities()
        .stream()
        .map(GrantedAuthority::getAuthority)
        .anyMatch(isAdminAuthority);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean hasGlobalOrCustomAdminAuthority(final Authentication authentication,
      final String adminAuthority) {
    final Predicate<String> isAdminAuthority = a -> Arrays
        .asList(GLOBAL_ADMIN_AUTHORIZATION, adminAuthority).contains(a);
    return authentication.getAuthorities().stream().map(GrantedAuthority::getAuthority)
        .anyMatch(isAdminAuthority);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isCurrentUserId(final Authentication authentication, final String userId) {
    return Optional.ofNullable(AuthUtils.principalToCustomerIdMapper.apply(authentication))
        .filter(currentId -> currentId.equals(userId)).isPresent();
  }
}
