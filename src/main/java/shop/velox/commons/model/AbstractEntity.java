package shop.velox.commons.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.validation.constraints.NotNull;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;


@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class AbstractEntity implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @Column(name = "PK")
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
  @GenericGenerator(name = "native", strategy = "native")
  private Long pk;

  @CreatedDate
  @NotNull
  @Column(name = "CREATION_TS", nullable = false, updatable = false)
  @Temporal(javax.persistence.TemporalType.TIMESTAMP)
  private Date createTime;

  @LastModifiedDate
  @NotNull
  @Temporal(javax.persistence.TemporalType.TIMESTAMP)
  @Column(name = "MODIFIED_TS", nullable = false)
  private Date modifiedTime;

  public Long getPk() {
    return pk;
  }

  public Date getCreateTime() {
    return createTime;
  }

  public Date getModifiedTime() {
    return modifiedTime;
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj == null) {
      return false;
    }
    if (obj == this) {
      return true;
    }
    if (obj.getClass() != getClass()) {
      return false;
    }
    AbstractEntity rhs = (AbstractEntity) obj;
    return new EqualsBuilder()
        .append(pk, rhs.pk)
        .isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder(17, 37)
        .append(pk)
        .toHashCode();
  }
}

