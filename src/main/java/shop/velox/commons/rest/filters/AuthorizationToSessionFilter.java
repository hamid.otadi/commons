package shop.velox.commons.rest.filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

@Component
@Order(1)
public class AuthorizationToSessionFilter implements Filter {

  private static final Logger LOG = LoggerFactory.getLogger(AuthorizationToSessionFilter.class);

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
      throws IOException, ServletException {
    HttpServletRequest req = (HttpServletRequest) request;
    String authorizationHeader = req.getHeader(HttpHeaders.AUTHORIZATION);
    HttpSession session = req.getSession();
    session.setAttribute(HttpHeaders.AUTHORIZATION, authorizationHeader);
    chain.doFilter(request, response);
  }
}
