package shop.velox.commons.rest.handler;

import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class RestResponseEntityExceptionHandler {

  @ExceptionHandler({AccessDeniedException.class})
  public void handleAccessDeniedException(final Exception ex, final HttpServletResponse response)
      throws IOException {
    response.sendError(HttpStatus.FORBIDDEN.value(), ex.getMessage());
  }

}
