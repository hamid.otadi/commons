package shop.velox.commons;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Local HTTP Basic auth configuration used if OIDC authentication is not enabled
 */
@EnableWebSecurity
@Order(110)
@ConditionalOnProperty(value = "velox.security.oidc.enabled", havingValue = "false", matchIfMissing = true)
public class VeloxLocalWebSecurityConfiguration extends AbstractVeloxWebSecurityConfiguration {

  @Override
  protected void configure(final HttpSecurity http) throws Exception {
    super.configure(http);
    http
        .httpBasic();
  }

  @Override
  protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
    auth.inMemoryAuthentication()
        .withUser("customer").password(passwordEncoder().encode("velox")).authorities("User")
        .and()
        .withUser("other_customer").password(passwordEncoder().encode("velox")).authorities("User")
        .and()
        .withUser("admin").password(passwordEncoder().encode("velox")).authorities("Admin")
        .and()
        .withUser("cart_admin").password(passwordEncoder().encode("velox"))
        .authorities("Admin_Cart");
  }

  @Bean
  public PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

}
